// 2024 Arnaud Champollion, licence GNU/GPL 3.0

// Constituants de la page
const body = document.body;
const inputPhrase = document.getElementById('phrase');
const boutonEntendre = document.getElementById('bouton-entendre');
const boutonCopier = document.getElementById('bouton-copier');
const paragrapheLien = document.getElementById('lien');
const zoneLien = document.getElementById('zone-lien');
const divErreur=document.getElementById('div-erreur');
const divApropos=document.getElementById('div-a-propos');
const divQrcode=document.getElementById('div-qrcode');
const divBlur=document.getElementById('blur');
const zoneQrcode=document.getElementById('zone-qrcode');
const texteErreur=document.getElementById('texte-erreur');
const compteur=document.getElementById('compteur');


//Adresse du site
const origin = window.location.origin;
const pathName = window.location.pathname;
let debutLien;
if (pathName.startsWith('http')) {
    debutLien = origin + pathName;
} else {
    debutLien = 'https://educajou.forge.apps.education.fr/lispourmoi';
}
console.log("Adresse de référence pour les liens : "+debutLien);

//Limite du nombre de caractères
limite = 1000;
inputPhrase.maxlenght = limite;

//Paramètres du QR-code
let qrcode = new QRCodeStyling({
    width: 500,
    height: 500,
    data: debutLien, // Le lien ou le texte que vous souhaitez encoder
    image: "images/favicon.svg",
    dotsOptions: {
        color: "#000", // Couleur des points du QR code
        type: "round" // Type de points (square, round, etc.)
    },
    backgroundOptions: {
        color: "#fff" // Couleur de fond du QR code
    }
});
qrcode.append(zoneQrcode);
let qrCodeElement = document.querySelector('#zone-qrcode canvas');

// Paramètres de langue
phraseAprononcer = new SpeechSynthesisUtterance('coucou');
phraseAprononcer.pitch=1;
phraseAprononcer.rate=0.8;
phraseAprononcer.lang='fr-FR';

// Lecture de l'URL
let url = new URL(window.location.href);


if (url.searchParams.get('primtuxmenu')==="true") {
    body.style.background='none';
    body.style.backgroundImage='url(images/primtux.png)';
}

// Vérification d'un texte présent dans l'URL
function test_url() {
    hash = window.location.hash;
    if (hash) {return true}
    else {return false}
}

// Si phrase dans l'URL, on l'insère dans la zone de saisie.
if (test_url()){
    let chaine_a_examiner = decodeURIComponent(hash.substring(1));
    inputPhrase.value = chaine_a_examiner;
    entendre(); // On lance automatiquement l'écoute.
}

// Action du bouton "entendre"
function entendre(){
    // Si une phrase est en train d'être prononcée, arrête-la
    if(speechSynthesis.speaking) {
        speechSynthesis.cancel();
    } else {
        majSyntheseVocale(inputPhrase.value).then(() => {
            boutonEntendre.classList.add('bouton-actif');
            inputPhrase.classList.add('actif');
            speechSynthesis.speak(phraseAprononcer);
            phraseAprononcer.onend = () => {
                boutonEntendre.classList.remove('bouton-actif');
                inputPhrase.classList.remove('actif');
            };
        });
    }
}

// Mise à jour de la synthèse vocale
async function majSyntheseVocale(phrase){
    phraseAprononcer.text = phrase;
}

// Mise à jour du lien
function majLien(phrase){

    if (phrase != ''){
        
        updateCounter(phrase);

        let phraseLien = encodeURIComponent(phrase);
        lien = debutLien+'#'+phraseLien;

        paragrapheLien.innerHTML=lien;



        zoneLien.classList.remove('hide');

    } else {
        paragrapheLien.innerHTML='';
        zoneLien.classList.add('hide');
    }

}

// Mise à jour du nombre de carctères restants dans la zone de saisie
function updateCounter(phrase) {
    let caracteresRestants = limite - phrase.length;
    if (caracteresRestants > 1){compteur.textContent = caracteresRestants + ' caractères restants';}
    else {compteur.textContent = caracteresRestants + ' caractère restant';}
}

// Fonction "copier le lien"
function copier(){
    navigator.clipboard.writeText(paragrapheLien.innerText);
}


// Fonction "télécharger le qRcode"
function telechargerQrcode() {
    // Créer un élément de lien temporaire
    let link = document.createElement('a');

    // Convertir le QR code en URL de données (data URL)
    link.href = qrCodeElement.toDataURL();

    // Spécifier le nom du fichier à télécharger
    let nomDuFichier='qrcode_'+nettoyerNomFichier(inputPhrase.value.substring(0, 25))+'.png';
    link.download = nomDuFichier;

    // Cliquez sur le lien pour déclencher le téléchargement
    link.click();
}

// Fonction Qrcode
function qrCode(){
    qrcode.update({ data: lien });
    qrCodeElement = document.querySelector('#zone-qrcode canvas');
    divQrcode.classList.remove('hide');
    divBlur.classList.remove('hide');
}

function nettoyerNomFichier(nomFichier) {
    // Liste des caractères non admis dans les noms de fichiers
    var caracteresInterdits = /[\\/:\*\?"<>\|]/g;
    // Supprimer les caractères non admis
    var nomFichierNettoye = nomFichier.replace(caracteresInterdits, '');
    // Supprimer les points ou espaces en fin de nom de fichier
    nomFichierNettoye = nomFichierNettoye.trimEnd().replace(/\.*$/, '');
    return nomFichierNettoye;
}

// À propos
function aPropos(){
    divApropos.classList.toggle('hide');
}

// Fermer une boîte de dialogue
function fermer(boite){
    boite.classList.add('hide');
    divBlur.classList.add('hide');
}

// Lancement du panneau de partage de l'appareil, si pas possible ouverture du client mail
function partager(){
    if (navigator.share) {
        navigator.share({
          title: "Lis pour moi",
          text: inputPhrase.value,
          url: paragrapheLien.innerText
        })
        .then(() => console.log('Partage réussi'))
        .catch((error) => console.log('Erreur de partage:', error));
      } else {
        const emailSubject = encodeURIComponent('Phrase à écouter sur l\'application "Lis pour moi"');
        const emailBody = encodeURIComponent('Bonjour,\n\nCliquez sur le lien ci-dessous pour écouter cette phrase.\n\n'+inputPhrase.value+'\n\n'+paragrapheLien.innerText);
        console.log(emailBody)
        const mailtoLink = `mailto:?subject=${emailSubject}&body=${emailBody}`;
        window.location.href = mailtoLink;
      }
}

// Vérifier si SpeechSynthesis est pris en charge
if ('speechSynthesis' in window) {
    // Charger les voix disponibles
    window.speechSynthesis.onvoiceschanged = function() {
        var voices = window.speechSynthesis.getVoices();
        var frenchVoiceAvailable = voices.some(function(voice) {
            return voice.lang.includes('fr');
        });
                
        if (!frenchVoiceAvailable) {
            texteErreur.innerText='Aucune voix française disponible sur ce navigateur';
            divErreur.classList.remove('hide');
        }
    };

} else {
    texteErreur.innerText="Aucune voix de synthèse n'est installée sur ce navigateur.";
    divErreur.classList.remove('hide');
}

// Pour empêcher l'utilisateur de faire glisser les images
document.querySelector('img').addEventListener('dragstart', function(event) {
    event.preventDefault(); // Empêcher le glissement par défaut
});